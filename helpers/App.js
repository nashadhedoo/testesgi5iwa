const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const twig = require('twig');


class App {

    static init(app, sequelize) {
        const User = require('../models/user')(sequelize);
        const Vehicle = require('../models/vehicle')(sequelize);
        const Immat = require('../models/immat')(sequelize);

        Immat.belongsTo(Vehicle);
        Vehicle.hasMany(Immat);

        const userRouter = require('../routes/user')({User});
        const vehicleRouter = require('../routes/vehicle')({Vehicle});
        const immatRouter = require('../routes/immat')({Immat});

        twig.cache(false);
        app.disable('x-powered-by');

        app.set('views', __dirname + '/../views');
        app.set('twig options', {
            allow_async: true,
            strict_variables: false
        });

        app.use(cors());
        app.use(bodyParser.urlencoded({extended: true}));
        app.use(bodyParser.json())
        app.use('/static', express.static(__dirname + '/../public'));

        app.get('/', function (req, res) {
            res.render('index.twig');
        });

        app.use('/api/users', userRouter);
        app.use('/api/vehicles', vehicleRouter);
        app.use('/api/immat', immatRouter);

        return app;
    }

    static serverListen(app) {
        const port = process.env.PORT || 3000;
        app.listen(port, () => console.info(`http://localhost:${port}`));
    }

}

module.exports = App;