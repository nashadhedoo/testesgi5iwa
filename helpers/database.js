const {Sequelize} = require('sequelize');

class Database {

    static async init() {
        const sequelize = new Sequelize('postgres://user:password@localhost:5500/api', {logging: false});

        try {
            await sequelize.authenticate();
        } catch (error) {
            console.error('Probleme de connexion Ã  la db:', error);
        }

        return sequelize;
    }

}

module.exports = Database;