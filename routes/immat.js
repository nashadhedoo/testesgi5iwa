const express = require('express');
const router = express.Router();
const {ErrValide} = require('sequelize');
const Auth = require('../middlewares/auth');


module.exports = function ({Immat}) {
    router.get('/', Auth.tokenAuth, async (req, res) => {
        res.json(await Immat.findAll());
    });

    router.post('/', Auth.tokenAuth, async (req, res) => {
        if (!req.body.name || !req.body.plaque || !req.body.vehicleId) {
            return res.status(400).send('Bad request');
        }

        try {
            const immat = await Immat.create({
                name: req.body.name,
                plaque: req.body.plaque,
                vehicleId: req.body.vehicleId
            });

            res.status(201).json(immat);
        } catch (error) {
            if (error instanceof ErrValide) {
                return res.status(400).json(error.errors[0].message);
            } else if (error.name === 'SequelizeForeignKeyConstraintError') {
                return res.status(400).json(error.detail);
            }
            console.error(error);
            res.sendStatus(500);
        }
    });

    router.get('/:id', Auth.tokenAuth, async (req, res) => {
        const id = parseInt(req.params.id);

        if (isNaN(id)) {
            return res.sendStatus(404);
        }

        try {
            const immat = await Immat.findByPk(id);

            if (immat) {
                res.json(immat);
            } else {
                res.sendStatus(404);
            }
        } catch (error) {
            console.error(error);
            res.sendStatus(500);
        }
    });

    router.delete('/:id', Auth.tokenAuth, async (req, res) => {
        const id = parseInt(req.params.id);

        if (isNaN(id)) {
            return res.sendStatus(404);
        }

        try {
            const remove = await Immat.destroy({where: {id}});

            if (remove) {
                res.sendStatus(204);
            } else {
                res.sendStatus(404);
            }
        } catch (error) {
            console.error(error);
            res.sendStatus(500);
        }
    });

    router.put('/:id', Auth.tokenAuth, async (req, res) => {
        const identificator = parseInt(req.params.id);

        if (isNaN(identificator)) {
            return res.sendStatus(404);
        }

        try {
            const [updated, [immat]] = await Immat.update(req.body, {where: {identificator}, returning: true});

            if (updated) {
                res.json(immat);
            } else {
                res.sendStatus(404);
            }
        } catch (error) {
            console.error(error);
            res.sendStatus(500);
        }
    });

    return router;
}
