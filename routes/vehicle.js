const express = require('express');
const router = express.Router();
const Auth = require('../middlewares/auth');

module.exports = function ({Vehicle}) {
    router.get('/', Auth.tokenAuth, async (req, res) => {
        res.json(await Vehicle.findAll());
    });

    router.post('/', Auth.tokenAuth, async (req, res) => {
        if (!req.body.name) {
            return res.status(400).send('Bad request');
        }

        try {
            const vehicle = await Vehicle.create({name: req.body.name});

            res.status(201).json(vehicle);
        } catch (error) {
            console.error(error);
            res.sendStatus(500);
        }
    });

    router.get('/:id', Auth.tokenAuth, async (req, res) => {
        const id = parseInt(req.params.id);

        if (isNaN(id)) {
            return res.sendStatus(404);
        }

        try {
            const vehicle = await Vehicle.findByPk(id);

            if (vehicle) {
                res.json(vehicle);
            } else {
                res.sendStatus(404);
            }
        } catch (error) {
            console.error(error);
            res.sendStatus(500);
        }
    });

    router.delete('/:id', Auth.tokenAuth, async (req, res) => {
        const id = parseInt(req.params.id);

        if (isNaN(id)) {
            return res.sendStatus(404);
        }

        try {
            const deleted = await Vehicle.destroy({where: {id}});

            if (deleted) {
                res.sendStatus(204);
            } else {
                res.sendStatus(404);
            }
        } catch (error) {
            console.error(error);
            res.sendStatus(500);
        }
    });

    router.put('/:id', Auth.tokenAuth, async (req, res) => {
        const id = parseInt(req.params.id);

        if (isNaN(id)) {
            return res.sendStatus(404);
        }

        try {
            const [updated, [vehicle]] = await Vehicle.update(req.body, {where: {id}, returning: true});

            if (updated) {
                res.json(vehicle);
            } else {
                res.sendStatus(404);
            }
        } catch (error) {
            console.error(error);
            res.sendStatus(500);
        }
    });

    return router;
}
