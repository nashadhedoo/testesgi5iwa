const {DataTypes,Model} = require('sequelize');

module.exports = function (sequelize) {
    class Immat extends Model {
    }

    Immat.init({
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        plaque: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                min: 0,
            }
        }
    }, {sequelize, modelName: 'immat'});

    return Immat;
};
