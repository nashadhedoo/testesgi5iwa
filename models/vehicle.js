const {Model, DataTypes} = require('sequelize');

module.exports = function (sequelize) {
    class Vehicle extends Model {
    }

    Vehicle.init({
        name: {
            type: DataTypes.STRING,
            allowNull: false
        }
    }, {sequelize, modelName: 'vehicle'});

    return Vehicle;
};
