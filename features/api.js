const {
  BeforeAll,
  AfterAll,
  Given,
  Then,
  When,
} = require("@cucumber/cucumber");
const express = require("express");
const { expect } = require("expect");
const App = require("../helpers/app");
const Database = require("../helpers/database");
const supertest = require("supertest");
const userFixture = require("../tests/fixtures/user.fixture");
const vehicleFixture = require("../tests/fixtures/vehicle.fixture");

let sequelize, request, references, user, vehicle;

BeforeAll(async function () {
  sequelize = await Database.init();
  sequelize.constructor._cls = new Map();
  sequelize.constructor._cls.set("transaction", await sequelize.transaction());

  user = await userFixture(sequelize);
  vehicle = await vehicleFixture(sequelize);

  references = {};
  references["vehicle.id"] = vehicle.id;

  request = supertest(App.init(express(), sequelize));

  const response = await request
    .post("/api/users/login")
    .set("Content-Type", "application/json")
    .send({
      username: "MBLH",
      password: "WASSMBLHNASH",
    });

  user.token = response.body.token;
});

AfterAll(async () => {
  await sequelize.constructor._cls.get("transaction").rollback();
  sequelize.close();
});

Given("reference: {string}", function (reference) {
  this.references = [];
  this.references.push(reference);
});

Given("payload ok", function (dataTable) {
  dataTable.rawTable = dataTable.rawTable.map((item) => {
    if (this.references.includes(item[1])) {
      item[1] = references[item[1]];
    }

    if (!isNaN(item[1])) {
      item[1] = parseInt(item[1]);
    }

    return item;
  });

  this.payload = dataTable.rowsHash();
});

When("{string} {string}", async function (method, path) {
  this.response = await request[method.toLowerCase()](path)
    .set("Authorization", "Bearer " + user.token)
    .set("Content-Type", "application/json")
    .send(this.payload);
});

When("I request {string} {string}", async function (method, path) {
  this.response = await request[method.toLowerCase()](path)
    .set("Authorization", "Bearer " + user.token)
    .set("Content-Type", "application/json")
    .send();
});

Then("reponse:  {int}", function (statusCode) {
  expect(this.response.status).toBe(statusCode);
});

Then("attribut", function (dataTable) {
  dataTable.rawTable = dataTable.rawTable.map((item) => {
    if (this.references.includes(item[1])) {
      item[1] = references[item[1]];
    }

    if (!isNaN(item[1])) {
      item[1] = parseInt(item[1]);
    }

    return item;
  });

  const expected = dataTable.rowsHash();
  const actual = this.response.body;

  expect(typeof actual).toBe("object");
  Object.keys(expected).forEach((key) =>
    expect(actual).toHaveProperty(key, expected[key])
  );
});

Then("attribut {string}", function (attr) {
  expect(this.response.body).toHaveProperty(attr);
});

Then("objetvide", function () {
  expect(Object.keys(this.response.body)).toHaveLength(0);
});

Then("{int} ", function (int) {
  expect(Object.keys(this.response.body)).toHaveLength(int);
});
