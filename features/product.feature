Feature: Immat

  Scenario: I should see all Immat
    When I request 'GET' '/api/immats'

  Scenario: I should create a new Immat
    Given I have reference 'vehicle.id'
    And I have payload
      | name    | Opel |
      | plaque   | FX601SP       |
      | vehicleId | vehicle.id  |
    When I request 'POST' '/api/immats' with payload
    Then The response status should be 201
    And I should have the 'id' attribute
    And I should have an object with the following attributes
      | name    | Opel |
      | plaque   | FX601SP       |
      | vehicleId | vehicle.id  |

  @Fixture
  Scenario: I should not retrieve the immat unknown
    When I request 'GET' '/api/immats/-1'
    Then The response status should be 404
    And I should have an empty object
