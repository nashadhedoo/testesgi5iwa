const supertest = require('supertest');
const express = require('express');
const App = require('../helpers/app');
const Database = require('../helpers/database');
const userFixture = require('./fixtures/user.fixture');
const vehicleFixture = require('./fixtures/vehicle.fixture');

let sequelize, request, user, vehicle;

beforeAll(async () => {
    sequelize = await Database.init();

    request = supertest(App.init(express(), sequelize));
});

beforeEach(async () => {
    sequelize.constructor._cls = new Map();
    sequelize.constructor._cls.set('transaction', await sequelize.transaction());

    user = await userFixture(sequelize);
    vehicle = await vehicleFixture(sequelize);

    const response = await request.post('/api/users/login').set('Content-Type', 'application/json').send({
        username: 'MBLH',
        password: 'WASSMBLHNASH'
    });

    user.token = response.body.token;
});

afterEach(async () => {
    await sequelize.constructor._cls.get('transaction').rollback();
});

afterAll(async () => {
    sequelize.close();
});

const authenticatedRequest = (endpoint, method = 'get') => {
    return request[method](endpoint).set('Authorization', 'Bearer ' + user.token);
}

describe('Vehicle', () => {
    it('should get all Vehicle', async () => {
        const response = await authenticatedRequest('/api/vehicles').send();

        expect(response.status).toBe(200);
        expect(response.body).toHaveLength(1);
    });

    it('should create a Vehicle', async () => {
        const response = await authenticatedRequest('/api/vehicles','post').set('Content-Type', 'application/json').send({
            name: 'Opel',
        });

        expect(response.status).toBe(201);
        expect(response.body).toHaveProperty('id');
        expect(response.body).toHaveProperty('name', 'Opel');
    });

    it('should has Vehicle', async () => {
        const response = await authenticatedRequest('/api/vehicles/' + vehicle.id).send();

        expect(response.status).toBe(200);
        expect(response.body).toHaveProperty('id');
        expect(response.body).toHaveProperty('name', 'Opel');
    });

    it('should update a Vehicle', async () => {
        const response = await authenticatedRequest('/api/vehicles/' + vehicle.id,'put').set('Content-Type', 'application/json').send({
            name: 'Peugeot',
        });

        expect(response.status).toBe(200);
        expect(response.body).toHaveProperty('id');
        expect(response.body).toHaveProperty('name', 'Peugeot');
    });

    it('should remove a Vehicle', async () => {
        const response = await authenticatedRequest('/api/vehicles/' + vehicle.id,'delete').send()

        expect(response.status).toBe(204);
    });
});
