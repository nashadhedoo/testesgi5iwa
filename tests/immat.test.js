const supertest = require('supertest');
const express = require('express');
const App = require('../helpers/app');
const Database = require('../helpers/database');
const userFixture = require('./fixtures/user.fixture');
const vehicleFixture = require('./fixtures/vehicle.fixture');
const immatFixture = require('./fixtures/immat.fixture');

let sequelize, request, user, vehicle, immat;

beforeAll(async () => {
    sequelize = await Database.init();

    request = supertest(App.init(express(), sequelize));
});

beforeEach(async () => {
    sequelize.constructor._cls = new Map();
    sequelize.constructor._cls.set('transaction', await sequelize.transaction());

    user = await userFixture(sequelize);
    vehicle = await vehicleFixture(sequelize);
    immat = await immatFixture(sequelize);

    const response = await request.post('/api/users/login').set('Content-Type', 'application/json').send({
        username: 'MBLH',
        password: 'WASSMBLHNASH'
    });

    user.token = response.body.token;
});

afterEach(async () => {
    await sequelize.constructor._cls.get('transaction').rollback();
});

afterAll(async () => {
    sequelize.close();
});

const authenticatedRequest = (endpoint, method = 'get') => {
    return request[method](endpoint).set('Authorization', 'Bearer ' + user.token);
}

describe('Immat', () => {
    it('RETOUR DES IMMATRICULATIONS', async () => {
        const response = await authenticatedRequest('/api/immats').send();

        expect(response.status).toBe(200);
        expect(response.body).toHaveLength(1);
    });

    it('Ne pas afficher sans token', async () => {
        const response = await request.get('/api/immats').send();

        expect(response.status).toBe(401);
    });

    it('Creation vehicule', async () => {
        let response = await authenticatedRequest('/api/immats', 'post').set('Content-Type', 'application/json').send({
            name: 'Immat',
            plaque: 'FX601SP',
            vehicleId: vehicle.id
        });

        expect(response.status).toBe(201);
        expect(response.body).toHaveProperty('id');
        expect(response.body).toHaveProperty('name', 'Immat');
        expect(response.body).toHaveProperty('plaque', 'FX601SP');
    });

    it('PAS DE CRÉATION SANS VÉHICULE DE CRÉER', async () => {
        const response = await authenticatedRequest('/api/immats', 'post').set('Content-Type', 'application/json').send({
            name: 'Immat',
            plaque: 'FX601SP',
        });

        expect(response.status).toBe(400);
    });

    it('Pas dimmat avev vehicule invalide', async () => {
        const response = await authenticatedRequest('/api/immats', 'post').set('Content-Type', 'application/json').send({
            name: 'Immat',
            plaque: 'FX601SP',
            vehicleId: vehicle.id + 1
        });

        expect(response.status).toBe(400);
    });

    it('Bah dimmat avec une plaque invalide', async () => {
        const response = await authenticatedRequest('/api/immats', 'post').set('Content-Type', 'application/json').send({
            name: 'Immat',
            plaque: 'FX601SP',
            vehicleId: vehicle.id
        });

        expect(response.status).toBe(400);
    });

    it('Pas de création avec immat invaliide', async () => {
        const response = await authenticatedRequest('/api/immats', 'post').set('Content-Type', 'application/json').send({
            name: 'Immat',
            vehicleId: vehicle.id
        });

        expect(response.status).toBe(400);
    });

    it('Pas de crea sans nom de vehicule', async () => {
        const response = await authenticatedRequest('/api/immats', 'post').set('Content-Type', 'application/json').send({
            plaque: 'FX601SP',
            vehicleId: vehicle.id
        });

        expect(response.status).toBe(400);
    });

    it('Pas de creation immat sans token', async () => {
        const response = await request.post('/api/immats').set('Content-Type', 'application/json').send({
            name: 'Immat',
            plaque: 'FX601SP',
            vehicleId: vehicle.id
        });

        expect(response.status).toBe(401);
    });

    it('IMMAT TEST', async () => {
        const response = await authenticatedRequest('/api/immats/' + immat.id).send();

        expect(response.status).toBe(200);
        expect(response.body).toHaveProperty('id');
        expect(response.body).toHaveProperty('name', 'Opel');
        expect(response.body).toHaveProperty('plaque', 'FX601SP');
    });

    it('Ne peux pas afficher si immat inexsistant', async () => {
        const response = await authenticatedRequest('/api/immats/' + immat.id + 1).send();

        expect(response.status).toBe(404);
    });

    it('Pas daffichage imat sans token', async () => {
        const response = await request.get('/api/immats/' + immat.id).send();

        expect(response.status).toBe(401);
    });



    it('pas update imat sans token', async () => {
        const response = await request.put('/api/immats/' + immat.id).set('Content-Type', 'application/json').send({
            name: 'Old immat'
        });

        expect(response.status).toBe(401);
    });

    it('suppression dune image', async () => {
        const response = await authenticatedRequest('/api/immats/' + immat.id, 'delete').send()

        expect(response.status).toBe(204);
    });

    it('pas de suppresion sans token', async () => {
        const response = await request.delete('/api/immats/' + immat.id).send()

        expect(response.status).toBe(401);
    });
});
