module.exports = async function (sequelize) {
    const Vehicle = require('../../models/immat')(sequelize);

    try {
        return await Vehicle.create({name: 'Opel', plaque: 'FX601SP'});
    } catch (error) {
        console.error(error);
        process.exit();
    }
}
