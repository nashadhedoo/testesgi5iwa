module.exports = async function (sequelize) {
    const Vehicle = require('../../models/vehicle')(sequelize);

    try {
        return await Vehicle.create({name: 'Opel'});
    } catch (error) {
        console.error(error);
        process.exit();
    }
}
