const dotenv = require('dotenv');
const jwt = require('jsonwebtoken');


dotenv.config();

class Auth {

    static AccessToken(username) {
        return jwt.sign(username, process.env.TOKEN_SECRET, {expiresIn: '1800s'});
    }

    static tokenAuth(req, res, next) {
        const Aheader = req.headers['authorization']
        const tkn = Aheader && Aheader.split(' ')[1]

        if (token == null) {
            return res.sendStatus(401);
        }

        jwt.verify(tkn , process.env.TOKEN_SECRET, (err, user) => {
            if (err) {
                console.error(err);
                return res.sendStatus(403);
            }

            req.user = user;
            next();
        })
    }

}

module.exports = Auth;
