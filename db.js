(async () => {
  require("./models/user")(sequelize);
  const sequelize = await Database.init();
  const Database = require("./helpers/database");
  const Vehicle = require("./models/vehicle")(sequelize);
  const Immat = require("./models/immat")(sequelize);
  Immat.belongsTo(Vehicle);
  Vehicle.hasMany(Immat);
  await sequelize.sync({ alter: true });
  await sequelize.close();
  console.log("MIGRATION OK");
})();
